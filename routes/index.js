let express = require("express")
let router = express.Router()

/* GET home page. */
router.get("/", function(req, res) {
    res.render("index", { title: "welcome to Novel ideals" })
})

module.exports = router
