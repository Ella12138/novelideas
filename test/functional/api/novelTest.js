const chai = require("chai")
const expect = chai.expect
const request = require("supertest")
const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer
const Novel = require("../../../models/novels")
const User = require("../../../models/user")
const Author = require("../../../models/author")
const mongoose = require("mongoose")
mongoose.set("useFindAndModify", false)
const {MongoClient} = require('mongodb')
const _ = require("lodash")

let server
let mongod
let db, validID,validID1,validID2
let url, connection, collection

describe("Novel-Ideas", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: "novelideas"
                }
            })
            url = await mongod.getConnectionString()

            connection = await MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            db = connection.db(await mongod.getDbName())
            collection = db.collection('novels')
            server = require("../../../bin/www")
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await connection.close()
            await mongod.stop()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await Novel.deleteMany({})
            let novel = new Novel()
            novel.name = "Rusty Hotel"
            novel.author = "Rusty"
            novel.type = "Horror"
            novel.recommender = "Merry"
            novel.grade = 5
            await novel.save()
            novel = new Novel()
            novel.name = "Cube Escape"
            novel.author = "Rusty"
            novel.type = "Horror"
            novel.recommender = "Merry"
            novel.grade = 3
            await novel.save()
            novel = await Novel.findOne({name: "Rusty Hotel"})
            validID = novel._id
        } catch (error) {
            console.log(error)
        }
    })
    describe("Novel", () => {
        describe("GET/novels", () => {
            it("should GET all the novels", done => {
                request(server)
                    .get("/novels")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        try {
                            expect(res.body).to.be.a("array")
                            expect(res.body.length).to.equal(2)
                            let result = _.map(res.body, novel => {
                                return {
                                    name: novel.name,
                                    author: novel.author,
                                    type: novel.type,
                                    recommender: novel.recommender,
                                }
                            })
                            expect(result).to.deep.include({
                                name: "Rusty Hotel",
                                author: "Rusty",
                                type: "Horror",
                                recommender: "Merry"

                            })
                            expect(result).to.deep.include({
                                name: "Cube Escape",
                                author: "Rusty",
                                type: "Horror",
                                recommender: "Merry"

                            })
                            done()
                        } catch (e) {
                            done(e)
                        }
                    })
            })
        })
        describe("GET /novels/:id", () => {
            describe("when the id is valid", () => {
                it("should return the matching novel", done => {
                    request(server)
                        .get(`/novels/${validID}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body[0]).to.have.property("name", "Rusty Hotel")
                            expect(res.body[0]).to.have.property("author", "Rusty")
                            expect(res.body[0]).to.have.property("type", "Horror")
                            expect(res.body[0]).to.have.property("recommender", "Merry")
                            done(err)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the NOT found message", done => {
                    request(server)
                        .get("/novels/1000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("Novel NOT Found!")
                            done(err)
                        })
                })
            })
        })
        describe("POST /novels", () => {
            it("should return can not be empty message", () => {
                const novel = {
                    name: "",
                    author: "Lily",
                    type: "Romantic",
                    recommender: "HP"
                }

                return request(server)
                    .post("/novels")
                    .send(novel)
                    .then(res => {
                        expect(res.body.message).equals("The novel name can not be empty")
                    })
            })
            it("should return novel already existed message", () => {
                const novel = {
                    name: "Rusty Hotel",
                    author: "Rusty",
                    type: "Horro",
                    recommender: "HP"
                }

                return request(server)
                    .post("/novels")
                    .send(novel)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("The novel is already exist")
                    })
            })
            it("should return confirmation message and update mongodb", () => {
                const novel = {
                    name: "My Girl",
                    author: "Lily",
                    type: "Romantic",
                    recommender: "HP"
                }

                return request(server)
                    .post("/novels")
                    .send(novel)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Novel Successfully added")
                        validID = res.body.data._id
                    })
            })
            after(() => {
                return request(server)
                    .get(`/novels/${validID}`)
                    .expect(200)
                    .then(res => {
                        expect(res.body[0]).to.have.property("name", "My Girl")
                        expect(res.body[0]).to.have.property("author", "Lily")
                        expect(res.body[0]).to.have.property("type", "Romantic")
                        expect(res.body[0]).to.have.property("recommender", "HP")
                    })
            })
        })
        describe("PUT /novels/:id", () => {
            describe("when the id is valid", () => {
                it("should return a message and update the grade", () => {
                    return request(server)
                        .put(`/novels/${validID}`)
                        .send({"grade": 4})
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Novel Successfully graded!"
                            })
                            expect(resp.body.data).to.have.property("grade", 4)
                        })
                })
                after(() => {
                    return request(server)
                        .get(`/novels/${validID}`)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body[0]).to.have.property("grade", 4)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the Novel NOT Found! message", done => {
                    request(server)
                        .get("/novels/1000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("Novel NOT Found!")
                            done(err)
                        })
                })
            })
        })
        describe("DELETE /novels/:id", () => {
            describe("when the id is valid", () => {
                it("should return confirmation message and update database", () => {
                    request(server)
                        .delete(`novels/${validID}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(res => {
                            expect(res.body.message).equals("Novels Successfully Deleted!")
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the NOT found message", () => {
                    request(server)
                        .delete("/novels/1000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(res => {
                            expect(res.body.message).equals("Novels NOT Found!")
                        })
                })
            })
        })
    })
})
