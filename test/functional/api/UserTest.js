const chai = require("chai")
const expect = chai.expect
const request = require("supertest")
const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer
const User = require("../../../models/user")
const mongoose = require("mongoose")
mongoose.set("useFindAndModify", false)
const {MongoClient} = require('mongodb')
const _ = require("lodash")

let server
let mongod
let db, validID1
let url, connection, collection
describe("Novel-Ideas", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: "novelideas"
                }
            })
            url = await mongod.getConnectionString()

            connection = await MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            db = connection.db(await mongod.getDbName())
            collection = db.collection('user')
            server = require("../../../bin/www")
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await connection.close()
            await mongod.stop()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {

            await User.deleteMany({})
            let user = new User()
            user.username = "Merry"
            user.password = "123456"
            user.email = "merry@ie"
            await user.save()
            user = new User()
            user.username = "Lily"
            user.password = "123456"
            user.email = "lily@ie"
            await user.save()
            user = await User.findOne({username: "Merry"})
            validID1 = user._id

        } catch (error) {
            console.log(error)
        }
    })
    describe("User", () => {
        describe("GET /user/:id", () => {
            describe("when the id is valid", () => {
                it("should return the matching user", done => {
                    request(server)
                        .get(`/user/${validID1}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body[0]).to.have.property("username", "Merry")
                            expect(res.body[0]).to.have.property("password", "123456")
                            expect(res.body[0]).to.have.property("email", "merry@ie")
                            done(err)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the NOT found message", done => {
                    request(server)
                        .get("/user/10001000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("User NOT Found!")
                            done(err)
                        })
                })
            })
        })
        describe("POST /user", () => {
            it("should return username can not be empty message", () => {
                const user = {
                    username: "",
                    password: "asdfgh",
                    email: "sth@wit"
                }
                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The username can not be empty")
                    })
            })
            it("should return password can not be empty message", () => {
                const user = {
                    username: "Justion",
                    password: "",
                    email: "sth@wit"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The password can not be empty")
                    })
            })
            it("should return email can not be empty message", () => {
                const user = {
                    username: "Justin",
                    password: "asdfgh",
                    email: ""
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The email can not be empty")
                    })
            })
            it("should return username occupied message", () => {
                const user = {
                    username: "Merry",
                    password: "asdfgh",
                    email: "sth@wit"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("The username is occupied")
                    })
            })
            it("should return confirmation message and update mongodb", () => {
                const user = {
                    username: "Justin",
                    password: "asdfgh",
                    email: "Justinsth@wit"
                }

                return request(server)
                    .post("/user")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("User Successfully registered")
                        validID1 = res.body.data._id
                    })
            })
            after(() => {
                return request(server)
                    .get(`/user/${validID1}`)
                    .expect(200)
                    .then(res => {
                        expect(res.body[0]).to.have.property("username", "Justin")
                        expect(res.body[0]).to.have.property("password", "asdfgh")
                        expect(res.body[0]).to.have.property("email", "Justinsth@wit")
                    })
            })
        })
        describe("POST /user/login", () => {
            it("should return username or password can not be empty message", () => {
                const user = {
                    username: "",
                    password: "asdfgh",
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .then(res => {
                        expect(res.body.message).equals("The username or password  can not be empty")
                    })
            })
            it("should return username is not exist message", () => {
                const user = {
                    username: "David",
                    password: "asdfgh",
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Username is not exist")
                    })
            })
            it("should return wrong password message", () => {
                const user = {
                    username: "Merry",
                    password: "asdfgh",
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Wrong Password")
                    })
            })
            it("should return confirmation message and update mongodb", () => {
                const user = {
                    username: "Merry",
                    password: "123456",
                }

                return request(server)
                    .post("/user/login")
                    .send(user)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Sign in Successfully")
                    })
            })
        })
        describe("PUT /user/:id", () => {
            describe("when the id is valid", () => {
                it("should return repeated password message", () => {
                    return request(server)
                        .put(`/user/${validID1}`)
                        .send({"password": "123456"})
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "No change to the Password"
                            })
                            expect(resp.body.data).to.have.property("password", "123456")
                        })
                })
                it("should return a message and update the password", () => {
                    return request(server)
                        .put(`/user/${validID1}`)
                        .send({"password": "123456abc"})
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Password Successfully changed!"
                            })
                            expect(resp.body.data).to.have.property("password", "123456abc")
                        })
                })
                after(() => {
                    return request(server)
                        .get(`/user/${validID1}`)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body[0]).to.have.property("password", "123456abc")
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the User NOT Found! message", done => {
                    request(server)
                        .get("/user/1000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("User NOT Found!")
                            done(err)
                        })
                })
            })
        })
    })
})
