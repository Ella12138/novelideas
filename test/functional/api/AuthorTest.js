const chai = require("chai")
const expect = chai.expect
const request = require("supertest")
const MongoMemoryServer = require("mongodb-memory-server").MongoMemoryServer
const Author = require("../../../models/author")
const mongoose = require("mongoose")
mongoose.set("useFindAndModify", false)
const {MongoClient} = require('mongodb')
const _ = require("lodash")

let server
let mongod
let db,validID2

let url, connection, collection

describe("Novel-Ideas", () => {
    before(async () => {
        try {
            mongod = new MongoMemoryServer({
                instance: {
                    port: 27017,
                    dbPath: "./test/database",
                    dbName: "novelideas"
                }
            })
            url = await mongod.getConnectionString()

            connection = await MongoClient.connect(url, {
                useNewUrlParser: true,
                useUnifiedTopology: true
            })
            db = connection.db(await mongod.getDbName())
            collection = db.collection('author')
            server = require("../../../bin/www")
        } catch (error) {
            console.log(error)
        }
    })

    after(async () => {
        try {
            await connection.close()
            await mongod.stop()
        } catch (error) {
            console.log(error)
        }
    })

    beforeEach(async () => {
        try {
            await Author.deleteMany({})
            let author = new Author()
            author.name = "Rusty"
            author.keyword1 = "Horror"
            author.keyword2 = "Science Fiction"
            author.numofbooks = 4
            author.numofcollected = 9
            await author.save()
            author = new Author()
            author.name = "White"
            author.keyword1 = "Romantic"
            author.keyword2 = "Science Fiction"
            author.numofbooks = 2
            author.numofcollected = 5
            await author.save()
            author = await Author.findOne({name: "Rusty"})
            validID2 = author._id
        } catch (error) {
            console.log(error)
        }
    })
    describe("Author", () => {
        describe("GET/author", () => {
            it("should GET all the authors", done => {
                request(server)
                    .get("/author")
                    .set("Accept", "application/json")
                    .expect("Content-Type", /json/)
                    .expect(200)
                    .end((err, res) => {
                        try {
                            expect(res.body).to.be.a("array")
                            expect(res.body.length).to.equal(2)
                            let result = _.map(res.body, author => {
                                return {
                                    name: author.name,
                                    keyword1: author.keyword1,
                                    keyword2: author.keyword2,
                                    numofbooks: author.numofbooks,
                                    numofcollected: author.numofcollected
                                }
                            })
                            expect(result).to.deep.include({
                                name: "Rusty",
                                keyword1: "Horror",
                                keyword2: "Science Fiction",
                                numofbooks: 4,
                                numofcollected: 9

                            })
                            expect(result).to.deep.include({
                                name: "White",
                                keyword1: "Romantic",
                                keyword2: "Science Fiction",
                                numofbooks: 2,
                                numofcollected: 5
                            })
                            done()
                        } catch (e) {
                            done(e)
                        }
                    })
            })
        })
        describe("GET /author/:id", () => {
            describe("when the id is valid", () => {
                it("should return the matching author", done => {
                    request(server)
                        .get(`/author/${validID2}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body[0]).to.have.property("name", "Rusty")
                            expect(res.body[0]).to.have.property("keyword1", "Horror")
                            expect(res.body[0]).to.have.property("keyword2", "Science Fiction")
                            expect(res.body[0]).to.have.property("numofbooks", 4)
                            expect(res.body[0]).to.have.property("numofcollected", 9)
                            done(err)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return the NOT found message", done => {
                    request(server)
                        .get("/author/100021000000020202")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .end((err, res) => {
                            expect(res.body.message).equals("Author NOT Found!")
                            done(err)
                        })
                })
            })
        })
        describe("POST /author", () => {
            it("should return can not be empty message", () => {
                const author = {
                    name: "",
                    keyword1: "Horror",
                    keyword2: "Science Fiction",
                    numofbooks: 4,
                }

                return request(server)
                    .post("/author")
                    .send(author)
                    .then(res => {
                        expect(res.body.message).equals("The author name can not be empty")
                    })
            })
            it("should return author already existed message", () => {
                const author = {
                    name: "Rusty",
                    keyword1: "Horror",
                    keyword2: "Science Fiction",
                    numofbooks: 4,
                }

                return request(server)
                    .post("/author")
                    .send(author)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("The author is already exist")
                    })
            })
            it("should return confirmation message and update mongodb", () => {
                const author = {
                    name: "Ella",
                    keyword1: "History",
                    keyword2: "Whodunit",
                    numofbooks: 3,
                }

                return request(server)
                    .post("/author")
                    .send(author)
                    .expect(200)
                    .then(res => {
                        expect(res.body.message).equals("Author Successfully added")
                        validID2 = res.body.data._id
                    })
            })
            after(() => {
                return request(server)
                    .get(`/author/${validID2}`)
                    .expect(200)
                    .then(res => {
                        expect(res.body[0]).to.have.property("name", "Ella")
                        expect(res.body[0]).to.have.property("keyword1", "History")
                        expect(res.body[0]).to.have.property("keyword2", "Whodunit")
                        expect(res.body[0]).to.have.property("numofbooks", 3)
                        expect(res.body[0]).to.have.property("numofcollected", 0)
                    })
            })
        })
        describe("PUT /author/:id/collect", () => {
            describe("when the id is valid", () => {
                it("should return a message and the author number of collected increased by 1", () => {
                    return request(server)
                        .put(`/author/${validID2}/collect`)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Author Successfully collected!"
                            })
                            expect(resp.body.data).to.have.property("numofcollected", 10)
                        })
                })
                after(() => {
                    return request(server)
                        .get(`/author/${validID2}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body[0]).to.have.property("numofcollected", 10)
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return a 404 and a message for invalid author id", () => {
                    return request(server)
                        .put("/author/11000100201/collect")
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Author NOT Found!"
                            })
                        })

                })
            })
        })
        describe("DELETE /author/:id", () => {
            describe("when the id is valid", () => {
                it("should return confirmation message", () => {
                    request(server)
                        .delete(`/author/${validID2}`)
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Author Successfully Deleted!"
                            })
                        })
                })
            })
            describe("when the id is invalid", () => {
                it("should return NOT found message", () => {
                    request(server)
                        .delete("/author/12001020100101")
                        .set("Accept", "application/json")
                        .expect("Content-Type", /json/)
                        .expect(200)
                        .then(resp => {
                            expect(resp.body).to.include({
                                message: "Author NOT Found!"
                            })
                        })
                })
            })
        })
    })
})
